// app.controller.ts
import { Controller, Inject } from '@nestjs/common';
import { ZBClient } from 'zeebe-node';
import { ZEEBE_CONNECTION_PROVIDER, ZeebeWorker, ZeebeServer } from '@payk/nestjs-zeebe';
import { AppService } from '../services/app.service';
import { RabbitmqPublisherService } from '../services/rabbitmq-publisher.service';

@Controller()
export class AppController {
  responses = new Map<string, Response>();

  constructor(
    @Inject(ZEEBE_CONNECTION_PROVIDER) private readonly zbClient: ZBClient,
    private readonly zeebeServer: ZeebeServer,
    private readonly appService: AppService,
    private readonly rabbitmqPublisherService: RabbitmqPublisherService, // publisher to submit result to gateway
  ) {
    // Publisher to submit result to gateway
    this.rabbitmqPublisherService.create()
      .then(() => console.log('Gateway publisher created'));
  }

  sendResultToGateway = (sessionId: string, result: any) =>
    this.rabbitmqPublisherService.publish({ sessionId, result })

  // Subscribe to events of type 'task-2'
  @ZeebeWorker('task-2')
  task2(job, complete) {
    console.log('task-2 -> Task variables', job.variables);

    // Task worker business logic
    const result = job.variables.result + '2';

    const variableUpdate = {
      tracer: 'task-2',
      status: 'ok',
      result,
      nextTask: 3,
    };

    complete.success(variableUpdate);
  }

  // Subscribe to events of type 'task-3'
  @ZeebeWorker('task-3')
  task3(job, complete) {
    console.log('task-3 -> Task variables', job.variables);

    // Task worker business logic
    const result = job.variables.result + '.3... ' + job.variables.sessionId;

    const variableUpdate = {
      tracer: 'task-3',
      status: 'ok',
      result,
    };

    complete.success(variableUpdate);

    this.sendResultToGateway(job.variables.sessionId, result);
  }

  // Subscribe to events of type 'task-4'
  @ZeebeWorker('task-4')
  task4(job, complete) {
    console.log('task-4 -> Task variables', job.variables);

    // Task worker business logic
    const result = job.variables.result + '.4... ' + job.variables.sessionId;

    const variableUpdate = {
      tracer: 'task-4',
      status: 'ok',
      result,
    };

    complete.success(variableUpdate);

    this.sendResultToGateway(job.variables.sessionId, result);
  }
}
